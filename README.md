# EAGLE Surface Brightness Generator
Generating SB of a list of EAGLE halos

## Installation

Using ```setyp.py```:
```bash
$ git clone https://gitlab.com/chore-assist/eagle_sb_generator.git
$ cd eagle_sb_generator
$ python3 setup.py install # --user in case you want to install it locally
```

Using ```pip```:
```bash
$ git clone https://gitlab.com/chore-assist/eagle_sb_generator.git
$ cd eagle_sb_generator
$ pip install . # --user in case you want to install it locally
$ pip3 install . # --user in case you want to install it locally
```

To install the package locally in editable mode, run:
```bash
$ pip install -e . --user
$ pip3 install -e . --user
```

## Usage

```
    >>> from eagle_sb_generator import <>
```


## Running tests

To execute tests, run the one of the following commands:
```bash
$ python3 ./setup.py test
```
or
```bash
$ py.test # -s to show stdout
```
from the root directory of the package.
