from setuptools import setup
from distutils.command.build_py import build_py as _build_py
import os

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


class CreateTestChombo(_build_py):
    '''Custom build command to create a test CHOMBO file'''

    def run(self):
        '''run `python setup.py chombo` to create a new test file'''
        _chombo.create()
        _build_py.run(self)


setup(
    name='eagle_sb_generator',
    version='0.0',
    description='A collection of useful models for different simulations',
    long_description=read('README.md'),
    url='http://gitlab.com/chore-assit/eagle_sb_generator',
    keywords='MyModels',
    author='Saeed Sarpas',
    author_email='saeed.sarpas@phys.ethz.ch',
    license='GPLv3',
    packages=['eagle_sb_generator'],
    install_requires=[
        'numpy',
        'astropy',
        'h5py',
        'matplotlib',
        'radamesh_py',
        'my_visualization'
    ],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    zip_safe=False
)
