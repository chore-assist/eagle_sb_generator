import astropy.units as u
from astropy.constants import c, m_p
from .uv_background import haardt_and_madau_uvbg
from .radamesh_source import RadameshSource
import itertools as it
import numpy as np
import subprocess
import h5py
import os, glob, shutil


def radamesh_runner_worker ( exe, chombo, cosmo, prefix, outputs ):
    """
    RadameshRunner worker
    Can be used by a multiprocessing pool
    """
    rr =  RadameshRunner ( exe, chombo, cosmo, output_prefix=prefix, n_outputs=outputs )

    rr.create_param_file ()
    rr.run ()

    return rr


def calculate_simulation_time ( box_size, cosmo, c_limit, f_HI=1e-4, factor=10 ):
    """
    Simulation time based on recombination and light crossing time scales
    """

    n_H = cosmo.Ob0 * cosmo.critical_density0 / m_p
    n_HI = n_H * f_HI
    n_e = n_H * (1 - f_HI)

    t_rec = ( 5 * 10**16 * u.s ) * n_HI / n_e
    t_cross =  ( box_size / c ) if c_limit else 0

    return factor * max ( t_rec, t_cross )


class RadameshRunner:
    """Radamesh Runner class"""

    def __init__ ( self, exe, input, cosmo, output_prefix='./', n_outputs=20,
        c_limit=True ):
        """
        Initializing RadameshRunner class

        Example:
        -------
        >>> from eagle_sb_generator.cosmology.eagle_cosmology import eagle_cosmology
        >>> from eagle_sb_generator.radamesh.radamesh_runner import RadameshRunner
        >>> cosmology = ealge_cosmology()
        >>> chombo = '/path/to/chombo/file'
        >>> rr = RadameshRunner (
        ...     'RADAMESH',
        ...     chombo,
        ...     cosmology,
        ...     output_prefix='./prefix/'
        ... )
        >>> rr.create_param_file ()
        >>> rr.run ()
        """
        rs = RadameshSource ()
        src = rs.borisova ()
        snap = h5py.File ( input, 'r' )

        box_size = snap['/'].attrs['ComovingBoxSize'] * u.Mpc
        redshift = snap['/'].attrs['Redshift']

        uv_bg = haardt_and_madau_uvbg ( redshift )
        simulation_time = calculate_simulation_time ( box_size, cosmo, c_limit )

        self.version_id = "_".join(os.path.basename( input ).split( '.' )[:-1])
        basename = "SO." + self.version_id + ".ts"

        self.path = {
            'exe': exe,
            'prefix': output_prefix,
            'basename': basename,
            'param': output_prefix + '/' + self.version_id + '.param',
            'log': output_prefix + '/' + self.version_id + '.log',
            'err': output_prefix + '/' + self.version_id + '.err',
            'globbing': output_prefix + '/' + self.version_id + '/' + basename + '*',
            'output_dir': output_prefix + '/' + self.version_id + '/',
            'files': []
        }

        # Directory structure
        if not os.path.isdir ( self.path['output_dir'] ):
            if not os.path.isdir ( self.path['prefix'] ):
                os.makedirs ( self.path['prefix'] )
            os.makedirs ( self.path['output_dir'] )


        self.params = {}
        self.params['runtime'] = [
            ( "ActualRun", ".true." ),
            ( "VersionID", self.version_id ),
            ( "Verbosity", "2" ),
            ( "DoInitEvol", ".true." ),
            ( "InitCollEq", ".true." ),
            ( "InitUVBEq", ".true." ),
            ( "LightSpeedLimit", (".true." if c_limit is True else ".false.") ),
            ( "InitialRedshift", redshift ),
            ( "InitialSimTime", "0.0" ),
            ( "SimulationTime", simulation_time.to(u.Myr).value ),
            ( "StarThr", "1.e19" ),
            ( "MaxNtrFrac", "0.0" )
        ]

        self.params['time_step'] = [
            ( "TimeStepFact", "0.1" ),
            ( "MinTimeStep", "0.04" )
        ]

        self.params['snapshot'] = [
            ( "DensityInputFile", '\"' + input + '\"' ),
            ( "ComovingBoxSize", box_size.to(u.Mpc).value ),
        ]

        self.params['cosmology'] = [
            ( "OmegaBaryonNow", cosmo.Ob0 ),
            ( "OmegaCDMNow", cosmo.Om0 ),
            ( "OmegaLambdaNow", cosmo.Ode0 ),
            ( "HubbleConstNow", cosmo.H0.value )
        ]

        self.params['background'] = [
            ( "Gamma_BKG", " ".join([ str(x) for x in [uv_bg[g] for g in ['GHI', 'GHeI', 'GHeII']]]) ),
            ( "HeatR_BKG", " ".join([ str(x) for x in [uv_bg[h] for h in ['HHI', 'HHeI', 'HHeII']]]) ),
            ( "KeepBKG", ".true." ),
            ( "UVB_SS", ".true." ),
            ( "UVB_SSThr", "0.005" )
        ]

        self.params['source'] = [
            ( "NSources", "1" ),
            ( "SourceCoords", "0.5 0.5 0.5" ),
            ( "SpectrumType", src['spectrum_type'] ),
            ( "TotalIonRate", np.log10 ( src['ion_rate'].to(u.Hz).value ) ),
            ( "SpectrumSlope", src['spectral_index'] ),
            ( "InitHIIradius", "1.e-4" ),
            ( "OpAngle", src['opening_angle'] )
        ]

        self.params['ray_tracing'] = [
            ( "NSpectralRegions", "3" ),
            ( "NSpectralBins", "10 5 5" ),
            ( "MinMaxEnergy", "1 1.8088 1.8088 4 4 8" ),
            ( "NRays", "5" ),
            ( "MaxTau", "50 50 50" ),
            ( "SafetyDist", "10.0" ),
            ( "InterpTauDiff", "0.1 1.e9 1.e9" ),
            ( "MinIntpDist", "0.02" ),
            ( "ConvThr", "0.1" ),
            ( "MinTau", "0.1 1.e9 1.e9" ),
            ( "ATSFreq", "5" ),
            ( "ATSFact", "1.e4" )
        ]

        output_times = np.logspace (
            0, np.log10(simulation_time.to(u.Myr).value + 1), n_outputs
        ) - 1

        self.params['output'] = [
            ( "NOutputs", n_outputs ),
            ( "OutputTime", " ".join([("%5.3f" % x) for x in output_times]) )
        ]


    def create_param_file ( self ):
        """
        Writing parameter file
        """
        options = list ( it.chain.from_iterable ( self.params.values() ) )

        f = open ( self.path['param'], 'w' )
        fmt = "%-" + str( max([len(opt[0]) for opt in options]) ) + "s"


        for sec, options in self.params.items():
            f.write ( '# ' + sec + '\n' )

            for opt in options:
                f.write ( fmt % opt[0] + " = " + str(opt[1]) + '\n' )

            f.write ( '\n' )

        f.close()


    def run ( self ):
        """
        Executing Radamesh as a subprocess
        """
        if not glob.glob ( self.path['globbing'] ):
            log_file = open ( self.path['log'], 'w' )
            err_file = open ( self.path['err'], 'w' )

            cmd = self.path['exe'] + " " + self.path['param']
            proc = subprocess.run ( cmd, shell=True, stderr=err_file, stdout=log_file )

            log_file.close ()
            err_file.close ()

        # Move snapshots to the output_directory
        for f in glob.glob ( './' + self.path['basename'] + '*' ):
            shutil.move ( f, self.path['output_dir'] )

        self.path['files'] = glob.glob ( self.path['globbing'] )
