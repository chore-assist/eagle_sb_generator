import astropy.units as u
from astropy.constants import c


class RadameshSource:
    """Radamesh Source class"""

    POWERLAW = 1
    BLACKBODY = 2

    def __init__ ( self, a_FUV=-0.61, a_EUV=-1.7, nu_break=(912 * u.angstrom) ):
        """
        Initializing RadameshSource class
        By default we are using Lusso et al. 2015 stacked QSED

        FUV: Far UV ( typically 2000A > lambda > 912A)
        EUV: Extreme UV ( typically 912A > lambda )
        """
        self.a_FUV = a_FUV
        self.a_EUV = a_EUV
        self.nu_break = nu_break


    def borisova ( self ):
        """Values from Borisova et al. 2016"""
        nu912 = c / (912 * u.angstrom)
        nu1700 = c / (1700 * u.angstrom)
        L1700 = 3 * 10**46 * (u.erg / u.s)

        a = self.a_EUV
        L = L1700 / nu1700**( a + 1 ) / ( a + 1 ) * ( - nu912**( a + 1 ) )
        ion_rate = L.to(u.eV / u.s) / (13.6 * u.eV)

        return {
            'L': L,
            'ion_rate': ion_rate,
            'spectral_index': a,
            'spectrum_type': RadameshSource.POWERLAW,
            'opening_angle': 0.0
        }


    def simple ( self, BHAccretionRate, efficiency=0.35, x=[0.5, 0.5, 0.5], spectrum_type='powerlaw' ):
        """
        A simple for model Quasar ESD

        parameters:
        ----------
        BHAccretionRate : Black Hole accretion Rate in M_sun per year (Astropy.Quantity)
        """

        self.efficiency = efficiency
        self.Luminocity = ( efficiency * BHAccretionRate * c**2 ).to(u.eV)

        pass
