import os, requests, csv, re
import numpy as np
import itertools as it


def gen_query ( dbname, snapnum, orderby, where ):
    query = "SELECT " + EagleAPI.ATTRS[0]

    for attr in EagleAPI.ATTRS[1:]:
        query += ", " + attr

    query += " FROM " + dbname + "_FoF as f, " + dbname + "_Subhalo as h "
    query += "WHERE h.SnapNum = " + snapnum + " "

    for w in where:
        query += "AND h." + w + " "

    query += "AND h.GroupID = f.GroupID "
    query += "ORDER BY f." + orderby

    return query


class EagleAPI:

    ATTRS = [
        "f.Redshift",
        "f.GroupID", "f.GroupMass",
        "f.Group_M_Crit200", "f.Group_R_Crit200",
        "f.Group_M_Mean200", "f.Group_R_Mean200",
        "f.GroupCentreOfPotential_x", "f.GroupCentreOfPotential_y", "f.GroupCentreOfPotential_z",
        "h.CentreOfMass_x", "h.CentreOfMass_y", "h.CentreOfMass_z",
        "h.CentreOfPotential_x", "h.CentreOfPotential_y", "h.CentreOfPotential_z",
        "h.Velocity_x", "h.Velocity_y", "h.Velocity_z",
        "h.BlackHoleMass", "h.BlackHoleMassAccretionRate",
        "h.GasSpin_x", "h.GasSpin_y", "h.GasSpin_z",
        "h.Mass", "h.MassType_BH", "h.MassType_DM", "h.MassType_Gas", "h.MassType_Star"
    ]

    URL = "http://galaxy-catalogue.dur.ac.uk:8080/Eagle"

    def __init__ ( self,
        dbname="RefL0025N0752",
        snapnum="12",
        where=[],
        orderby="Group_M_Crit200",
        output_prefix="./" ):
        """
        Initializing EagleAPI

        Example:
        -------
        >>> from eagle_sb_generator.api.eagle_api import EagleAPI
        >>> ea = EagleAPI ( where=[ "SubGroupNumber = 0", "Group_M_Mean200 > 1e11" ] )
        >>> ea.load ()
        >>> ea.halos[ 'Group_M_Crit200' ]
        """

        self.dbname = dbname
        self.snapnum = snapnum

        self.path = output_prefix + dbname + "-" + snapnum + "-" + "-".join(where) + ".txt"
        self.path = self.path.replace ( " ", "_" )

        self.query = gen_query ( dbname, snapnum, orderby, where )

        self.dtype = []
        self.nentries = 0
        self.nheader = 0 # Number of header lines
        self.halos = []

    def download ( self ):
        """
        Downloading the snapshot
        """
        if os.path.exists ( self.path ) is not True:
            # NOTE: url cannot be longer 1500 characters
            params = { 'action': 'doQuery', 'SQL': self.query }
            res = requests.get (
                EagleAPI.URL,
                params=params,
                auth=('ssarpas', os.environ['EAGLE_PASS']) )

            with open ( self.path, "w" ) as f:
                f.write ( res.text )


    def get_dtype ( self ):
        """
        Readig data types of different column from the download text file
        """
        if not os.path.exists ( self.path ):
            self.download()

        with open ( self.path ) as csvfile:
            csvreader = csv.reader ( csvfile, delimiter=',' )

            self.nheader = 0 # Number of header lines
            for row in csvreader:
                if row[0][0] == "#":
                    self.nheader += 1

                    if row[0][0:7] != "#COLUMN": continue

                    arr = re.split ( ' |=', row[0] )

                    if arr[-1] == "bigint" or arr[-1] == "int":
                        self.dtype.append ( ( arr[3], np.int64 ) )
                    elif arr[-1] == "real":
                        self.dtype.append ( ( arr[3], np.float64 ) )
                    elif arr[-1] == "nvarchar":
                        self.dtype.append ( ( arr[3], np.unicode_, 128 ) )


            csvfile.seek(0)
            self.nentries = sum ( 1 for line in csvfile ) - self.nheader

    def load ( self ):
        """
        Loading halos into self.halos
        """
        if not self.dtype:
            self.get_dtype ()

        self.halos = np.genfromtxt (
            self.path,
            dtype=self.dtype,
            delimiter=',',
            usecols=range( len(self.dtype) ),
            skip_header=self.nheader + 1 )
