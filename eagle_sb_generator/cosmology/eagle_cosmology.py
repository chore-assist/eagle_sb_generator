from astropy.cosmology import Planck13 as cosmo
from astropy.cosmology import LambdaCDM


def eagle_cosmology ():
    return LambdaCDM ( cosmo.H0, cosmo.Om0, cosmo.Ode0, Ob0=cosmo.Ob0 )
