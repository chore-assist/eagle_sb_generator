"""main.py"""
import multiprocessing as mp
import astropy.units as u
from astropy.constants import m_p
import itertools as it
import glob, os


from .api.eagle_api import EagleAPI
from .cosmology.eagle_cosmology import eagle_cosmology
from .p2c.p2c_runner import p2c_worker
from .radamesh.radamesh_runner import radamesh_runner_worker
from .sb_profile.surface_brightness_map import surface_brightness_map_worker


class EagleSBGenerator:
    """
    A collection of useful models

    Example:
    -------
    >>> from eagle_sb_generator import *
    >>> esb = EagleSBGenerator ()
    >>> esb.p2c ('P2C-1.2', '/path/to/snapshot', output_prefix='./cutted_halos', n_threads=36)
    >>> esb.radamesh ('Radamesh-1.3_64', output_prefix='./radamesh_output/', n_threads=36)
    >>> esb.radamesh_snaps
    """

    def __init__ ( self,
        dbname="RefL0025N0752",
        snapnum="12",
        lower_mass=1e11,
        upper_mass=1e14,
        output_prefix="./" ):
        """
        Initializing an instance of MyModel class
        """

        where = [
            "SubGroupNumber = 0",
            ( "Mass > %4.2e" % lower_mass ),
            ( "Mass < %4.2e" % upper_mass )
        ]

        self.eagle = EagleAPI ( dbname=dbname, snapnum=snapnum, where=where )
        self.eagle.load()

        self.cosmo = eagle_cosmology ()

        self.cutted_halos = []
        self.radamesh_snaps = []
        self.sb_maps = []


    def p2c ( self, exe, snapshot, n_threads=8, output_prefix='./' ):
        """
        Generating Chombo file of each halo
        """

        pool = mp.Pool ( processes = n_threads )

        pss = [
            pool.apply_async (
                p2c_worker,
                [ exe, snapshot, halo, output_prefix ]
            ) for halo in self.eagle.halos
        ]

        self.cutted_halos = [ ps.get() for ps in pss ]


    def radamesh ( self, exe, n_threads=8, output_prefix='./', n_outputs=20 ):
        """
        Running ray-tracing based on cutted halos (self.cutted_halos)
        """
        pool = mp.Pool ( processes = n_threads )

        pss = [
            pool.apply_async (
                radamesh_runner_worker,
                [ exe, chombo.output, self.cosmo, output_prefix, n_outputs ]
            ) for chombo in self.cutted_halos
        ]

        self.radamesh_snaps = [ ps.get() for ps in pss ]


    def surface_brightness ( self, C=1, X=.75, Y=.25,
        SF_number_density_thr=1, output_prefix='./', n_threads=8 ):
        """
        Plotting the surface brightness of halos in different projection
        directions, separately and together
        """

        SF_density_thr = SF_number_density_thr * m_p * (1 / u.cm**3)

        ts_files = list (
            it.chain.from_iterable (
                [ r.path['files'] for r in self.radamesh_snaps ]
            )
        )

        pool = mp.Pool (processes = n_threads )

        pss = [
            pool.apply_async (
                surface_brightness_map_worker,
                [ f, self.cosmo, output_prefix, C, X, Y,
                SF_number_density_thr * m_p * (1 / u.cm**3) ]
            ) for f in ts_files
        ]

        self.sb_maps = [ ps.get() for ps in pss ]
