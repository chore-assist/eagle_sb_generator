# From Hui & Genedin 1996 (Case A recombination)


def alpha_rec_a ( T ):
    l = 2 * 157807 / T
    return ( 1.269e-13 * l**1.503 / ( 1 + (l / 0.522)**0.470 )**1.923 ) # cm^3 s^-1


def alpha_rec_b ( T ):
    l = 2 * 157807 / T
    return ( 2.753e-14 * l**1.5 / ( 1 + (l / 2.740)**0.407 )**2.242 ) # cm^3 s^-1
