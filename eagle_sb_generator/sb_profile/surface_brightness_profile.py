import numpy as np
import astropy.units as u
from astropy.io import fits
from my_visualization import MyVisualization as Vis
import re


class SurfaceBrightnessProfile:
    """
    Surface Brightness Profile generator
    """

    def __init__ ( self, fits_files, minr=(10 * u.kpc), maxr=(1000 * u.kpc),
    nbins=20, output_prefix='./' ):
        """
        Initializing SurfaceBrightnessProfile class

        Example:
        -------
        >>> from eagle_sb_generator.sb_profile.surface_brightness_profile import SurfaceBrighnessProfile
        >>> import glob
        >>> fits_files = glob.glob ( '/path/to/fits/dir/*.fits' )
        >>> sbp = SurfaceBrighnessProfile ( fits_files )
        >>> sbp.calculate_distances ()
        >>> sbp.fill_bins ()
        """

        self.vis = Vis ()

        self.nbins = nbins
        self.rbin_edges = np.logspace (
            np.log10( minr.to(u.kpc).value ),
            np.log10( maxr.to(u.kpc).value ),
            nbins + 1
        )
        self.rbin_centers = [
            10**( (np.log10(bmin) + np.log10(bmax)) / 2 )
            for bmin, bmax in zip ( self.rbin_edges[:-1], self.rbin_edges[1:] )
        ]

        self.fits = [ fits.open (f) for f in fits_files ]

        self.boxsize = [
            float( f[0].header['boxsize'] ) * u.Mpc
            for f in self.fits
        ]

        self.dimensions = [ f[0].data.shape for f in self.fits ]

        self.path = {
            'prefix': output_prefix,
            'basenames': [
                os.splitext( os.path.split(f)[-1] )[0] + '_sb_profile.png'
                for f in fits_files
            ],
            'all': re.sub (
                '_ts[0-9]{4}', '',
                os.splitext( os.path.split(f)[-1] )[0]
            ) + '_sb_profile.png',
        }

        self.d_ckpc = []
        self.bins = [ [] for _ in range(self.nbins) ]


    def calculate_distances ( self ):
        indices = [ np.indices(d) - (d[0] - 1) / 2 for d in self.dimensions ]

        self.d_ckpc = [
            np.sqrt ( idx[0]**2 + idx[1]**2 ) * bs.to(u.kpc).value / d[0]
            for idx, bs, d in zip ( indices, self.boxsize, self.dimensions )
        ]


    def fill_bins ( self ):
        if not self.d_ckpc:
            self.calculate_distances ()

        for i, d_ckpc in enumerate ( self.d_ckpc ):
            self.bins[i] = [ [] for _ in range(self.nbins) ]
            self.__fill_bins ( self.bins[i], d_ckpc, self.fits[i] )


    def __fill_bins ( self, bins, d_ckpc, fits_file ):
        for dist, map in zip ( np.nditer(d_ckpc), np.nditer(fits_file[0].data) ):
            for bin, bmin, bmax in zip ( bins, self.rbin_edges[:-1], self.rbin_edges[1:] ):
                if bmin <= dist < bmax:
                    bin.append ( np.asscalar(map) )
                    break


    def single_profiles ( self ):
        """
        Plotting surface brightness profiles
        """
        for bins in self.bins:
            median = [ np.median ( bin ) for bin in bins ]
            mean = [ np.mean ( bin ) for bin in bins ]
            std = [ np.std ( bin ) for bin in bins ]
