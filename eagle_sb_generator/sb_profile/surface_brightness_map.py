from .giovanardi_1987 import q1_2p, q1_3d, q1_3s
from .hui_genedin_1996 import alpha_rec_a, alpha_rec_b
from .cantalupo_20017 import ly_alpha_production_efficiency_case_b_recom

from radamesh_py import RadameshPy as RP
from astropy.io import fits
from astropy.constants import m_p
import astropy.units as u
from math import pi
import numpy as np
import os, errno


def surface_brightness_map_worker ( input, cosmo, prefix, C, X, Y, SF_density_thr ):
    """
    SurfaceBrightnessMap worker
    Can be used by a multiprocessing pool
    """
    sbm = SurfaceBrightnessMap ( input, cosmo, output_prefix=prefix )
    sbm.load ()
    sbm.sb_maps ( C=C, X=X, Y=Y, SF_density_thr=SF_density_thr )
    sbm.save ()

    del sbm.rp
    del sbm.cosmo

    return sbm


class SurfaceBrightnessMap:
    def __init__ ( self, input, cosmo, output_prefix='./' ):
        """
        Generating surface brightness map of Radamesh outputs

        Example:
        -------
        >>> from eagle_sb_generator.sb_profile.surface_brightness_map import SurfaceBrightnessMap
        >>> from eagle_sb_generator.cosmology.eagle_cosmology import eagle_cosmology
        >>> cosmo = eagle_cosmology ()
        >>> sbm = SurfaceBrightnessMap (
        ...     '/path/to/radamesh/output',
        ...     cosmo,
        ...     output_prefix='/path/to/output/dir'
        ... )
        >>> sbm.load ()
        >>> sbm.sb_maps ()
        >>> sbm.save ()
        """

        nickname = os.path.split ( input )[-1]
        dirname = os.path.splitext ( nickname )[0]

        nickname = nickname.replace ( '.', '_' )
        dirname = dirname.replace ( '.', '_' )

        self.rp = RP ( input )
        self.cosmo = cosmo

        dirs = ['x', 'y', 'z']
        self.path = {
            'prefix': output_prefix,
            'output_dir': output_prefix + '/' + dirname + '/',
            'nickname': nickname,
            'sb_map_rec': [ nickname + ('_sb_map_rec_%c.fits' % c) for c in dirs ],
            'sb_map_col': [ nickname + ('_sb_map_col_%c.fits' % c) for c in dirs ],
            'sb_map': [ nickname + ('_sb_map_%c.fits' % c) for c in dirs ],
            'sb_map_rel_rec': [ nickname + ('_sb_map_relative_rec_%c.fits' % c) for c in dirs ],
            'sb_map_rel_col': [ nickname + ('_sb_map_relative_col_%c.fits' % c) for c in dirs ],
        }

        # Directory structure
        try:
            os.makedirs ( self.path['prefix'] )
        except OSError as e:
            pass

        try:
            os.makedirs ( self.path['output_dir'] )
        except OSError as e:
            pass

        self.rmaps = []
        self.cmaps = []
        self.maps = []
        self.rrelmaps = []
        self.crelmaps = []


    def load ( self ):
        """
        Loading Radamesh snapshot
        """

        self.rp.load ()
        self.rp.fill_boxes ()


    def sb_maps ( self, C=1, X=.75, Y=.25, SF_density_thr=(m_p.to(u.g) * (1 / u.cm**3)) ):
        """
        Generating brightness cube
        For each cell, we calculate the brightness of the cell as:


        Parameters:
        ----------
        C : number
            Clumping factor
        SF_density_thr : Astropy.Quantity
            Star formation density threshold (M / L**3)
        """

        # NB: right now it only works on uniform chombo file
        box = self.rp.levels[0]['boxes'][0]

        if not box:
            self.load ()

        rho_b_crit = ( self.cosmo.Ob0 * self.cosmo.critical_density0 ).to(
            u.g / u.cm**3
        ).value

        SF_density_thr = SF_density_thr.to ( u.g / u.cm**3 ).value

        lya_per_arcsec2 = ( 10.19 / (4 * pi) * (u.eV / u.sr) ).to (
            u.erg / u.arcsec**2
        ).value

        rho = box['density']
        f_HI = box['f_HI']
        f_HeI = box['f_HeI']
        f_HeII = box['f_HeII']
        T = box['temperature']

        np.place ( rho, (rho * rho_b_crit) < SF_density_thr, rho * rho_b_crit )

        f_HII = 1 - f_HI
        f_HeIII = 1 - f_HeI - f_HeII

        mu_inv = X / 1.00794 + Y / 4.002602
        mp_g = m_p.to(u.g).value

        n_HI = X * f_HI * rho * mu_inv / mp_g
        n_HII = X * f_HII * rho * mu_inv / mp_g
        n_e = ( X * f_HII + Y * (f_HeII + 2 * f_HeIII) ) * rho * mu_inv / mp_g

        e_lya = ly_alpha_production_efficiency_case_b_recom ( T )
        rec_coeff = alpha_rec_b ( T )
        q_eff = q1_2p ( T ) + q1_3s ( T ) + q1_3d ( T )

        rcube = e_lya * rec_coeff * n_HII * n_e # Recombination
        ccube = q_eff * n_HI * n_e # Collisional excitation
        cube = rcube + ccube

        box_size = self.rp.comoving_box_size.to(u.cm).value
        dims = box['density'].shape
        f = [ C * (1+self.rp.z) * box_size / dims[i] * lya_per_arcsec2 for i in range(3) ]

        self.rmaps = [ np.apply_along_axis(np.sum, i, rcube) * f[i] for i in range(3) ]
        self.cmaps = [ np.apply_along_axis(np.sum, i, ccube) * f[i] for i in range(3) ]
        self.maps = [ np.apply_along_axis(np.sum, i, cube) * f[i] for i in range(3) ]
        self.rrelmaps = [ self.rmaps[i] / self.maps[i] for i in range(3) ]
        self.crelmaps = [ self.cmaps[i] / self.maps[i] for i in range(3) ]


    def save ( self ):
        """
        Generating surface brightness fits files
        """

        if not self.maps:
            self.sb_maps ()

        rmaps_fits = [ fits.PrimaryHDU ( self.rmaps[i] ) for i in range(3) ]
        cmaps_fits = [ fits.PrimaryHDU ( self.cmaps[i] ) for i in range(3) ]
        maps_fits = [ fits.PrimaryHDU ( self.maps[i] ) for i in range(3) ]
        rel_rmaps_fits = [ fits.PrimaryHDU ( self.rrelmaps[i] ) for i in range(3) ]
        rel_cmaps_fits = [ fits.PrimaryHDU ( self.crelmaps[i] ) for i in range(3) ]

        boxsize = self.rp.comoving_box_size.to(u.Mpc).value

        for map, name in zip ( rmaps_fits, self.path['sb_map_rec'] ):
            if not os.path.exists ( self.path['output_dir'] + name ):
                map.header.set('boxsize', boxsize)
                map.header.set('redshift', self.rp.z)
                map.writeto ( self.path['output_dir'] + name )

        for map, name in zip ( cmaps_fits, self.path['sb_map_col'] ):
            if not os.path.exists ( self.path['output_dir'] + name ):
                map.header.set('boxsize', boxsize)
                map.header.set('redshift', self.rp.z)
                map.writeto ( self.path['output_dir'] + name )

        for map, name in zip ( maps_fits, self.path['sb_map'] ):
            if not os.path.exists ( self.path['output_dir'] + name ):
                map.header.set('boxsize', boxsize)
                map.header.set('redshift', self.rp.z)
                map.writeto ( self.path['output_dir'] + name )

        for map, name in zip ( rel_rmaps_fits, self.path['sb_map_rel_rec'] ):
            if not os.path.exists ( self.path['output_dir'] + name ):
                map.header.set('boxsize', boxsize)
                map.header.set('redshift', self.rp.z)
                map.writeto ( self.path['output_dir'] + name )

        for map, name in zip ( rel_cmaps_fits, self.path['sb_map_rel_col'] ):
            if not os.path.exists ( self.path['output_dir'] + name ):
                map.header.set('boxsize', boxsize)
                map.header.set('redshift', self.rp.z)
                map.writeto ( self.path['output_dir'] + name )
