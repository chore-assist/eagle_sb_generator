# From Cantalupo et al. 2007


import numpy as np


def ly_alpha_production_efficiency_case_b_recom ( T ):
    """
    Ly-alpha production efficiency for case B recombination

    NB: 0.1% accurate in 1e2 K < T < 1e5 K
    """
    return 0.686 - 0.106 * np.log10 ( T / 1.e4 ) - 0.009 * ( T / 1.e4 )**-0.44
