# From Giovanardi et al. 1987


import numpy as np
from astropy.constants import m_p, k_B
import astropy.units as u

kb_eV = k_B.to ( u.eV / u.K ).value


def q1_2p ( T ):
    Gamma = 3.162e-1 + 1.472e-5 * T - 8.275e-12 * T**2 - 8.794e-19 * T**3
    # Gamma = 3.435e-1 + 1.297e-5 * T + 2.178e-12 * T**2 + 7.928e-17 * T**3
    return (8.629e-6 * Gamma * np.exp(-12.1 / (kb_eV * T)) / (np.sqrt(T) * 2)) # cm^3 s^-1


def q1_3s ( T ):
    Gamma = 3.337e-2 + 2.223e-7 * T - 2.794e-13 * T**2 + 1.516e-19 * T**3
    # Gamma = 6.250e-2 - 1.299e-6 * T + 2.666e-11 * T**2 - 1.596e-16 * T**3
    return (8.629e-6 * Gamma * np.exp(-12.6 / (kb_eV * T)) / (np.sqrt(T) * 8)) # cm^3 s^-1


def q1_3d ( T ):
    Gamma = 5.051e-2 + 7.876e-7 * T - 2.072e-12 * T**2 + 1.902e-18 * T**3
    # Gamma = 5.030e-2 + 7.514e-7 * T - 2.826e-13 * T**2 + 1.098e-17 * T**3
    return (8.629e-6 * Gamma * np.exp(-12.6 / (kb_eV * T)) / (np.sqrt(T) * 8)) # cm^3 s^-1
