from ..cosmology.eagle_cosmology import eagle_cosmology
from astropy import units as u
import subprocess
import os


def gen_output_path ( halo ):
    output = '/' + str(halo['GroupID']) + '-'
    output += ('%5.3e' % halo['Group_M_Mean200'])
    output += '.chombo.h5'

    return output


def gen_options ( input, output, halo, grid, pixel, cosmo ):
    CMs = [ 'CentreOfMass_x', 'CentreOfMass_y', 'CentreOfMass_z' ]

    # Multiplied by dimensionless Hubble parameter since the particle data is in this unit
    ledges = [ str( (halo[x] - grid / 2 * pixel.to(u.Mpc).value) * cosmo.h ) for x in CMs ]
    redges = [ str( (halo[x] + grid / 2 * pixel.to(u.Mpc).value) * cosmo.h ) for x in CMs ]

    cmd = ' -inp ' + input + ' -out ' + output
    cmd += ' -base_grid ' + str(grid)
    cmd += ' -lmax 0 '
    cmd += ' -cut_le ' + '"' + ','.join(ledges) + '"'
    cmd += ' -cut_re ' + '"' + ','.join(redges) + '"'

    return cmd


def calculate_base_grid ( fac, r_200, pixel ):
    ratio = ( (fac * r_200).to(u.Mpc) / pixel.to(u.Mpc) ).decompose().value
    return round ( ratio / 2 ) * 2


def calculate_pixel ( size_arcsec, z, cosmo ):
    return ( size_arcsec * u.arcsec ) / cosmo.arcsec_per_kpc_comoving (z)


def p2c_worker ( exe, snapshot, halo, output_prefix ):
    """
    P2C worker
    Can be used by a multiprocessing pools

    Example:
    -------
    >>> import multiprocessing as mp
    >>> pool = mp.Pool ( processes = n_threads )
    >>> pss = [
    ...     pool.apply_async (
    ...         p2c_worker,
    ...         [ exe, snapshot, halo, output_prefix ]
    ...     ) for halo in halos
    ... ]
    """
    p2c = P2CRunner ( exe, snapshot, halo, output_prefix )

    if not os.path.exists ( p2c.output ):
        p2c.run ()

    return p2c


class P2CRunner:
    """ P2C (Particle to Chombo) Runner """

    def __init__ ( self, exe, input, halo, output_prefix='./' ):
        """
        Initializing P2CRunner class

        Note: All lengths are comoving
        """
        cosmo = eagle_cosmology ()

        z = halo['Redshift']
        r_200 = halo['Group_R_Mean200'] * u.kpc * ( 1 + z ) # pkpc to ckpc
        muse_pixel = calculate_pixel ( 0.2, z, cosmo )
        grid = calculate_base_grid ( 4, r_200, muse_pixel )

        self.exe = exe
        self.output = output_prefix + gen_output_path ( halo )
        self.opts = gen_options ( input, self.output, halo, grid, muse_pixel, cosmo )


    def run ( self ):
        """
        Executing the command as a subprocess
        """
        log_file = open ( self.output + ".log", "w" )
        err_file = open ( self.output + ".err", "w" )

        cmd = self.exe + " " + self.opts
        proc = subprocess.run ( cmd, shell=True, stderr=err_file, stdout=log_file )
        print ( proc.args )

        log_file.close ()
        err_file.close ()
